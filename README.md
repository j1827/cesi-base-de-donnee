# Cours Yohan Parent

## A vous de Jouer

- 1.Qu’est ce qu’un système d’information (SI) ? Donner un exemple.
>**Un système d'information est un ensemble d'ordinateur(imprimante, téléphone) , mis en réseau**

- 2.Citer des exemples de logiciels rencontrés dans le monde de l’entreprise dans les domaines suivants : Ressources Humaines, Relation commerciale, Progiciel de gestion intégré (ERP). Quel est l’environnement technique que vous pouvez rencontrer avec ces exemples ?
>**Voici quelque logiciel pouvant etre rencontré dans le monde de l'entreprise : odoo, glpi, Gestsup**
- 3.Qu’est ce qu’une donnée au sens informatique ?
>**Une donnée au sens informatique est une information dans un programme et/ou dans un environement**
- 4.Comment pouvez-vous stocker cette donnée et la manipuler ? 
>**Les données peuvent etre stocké dans un fichier et la manipulation peut se faire via des programmes**
- 5.Au niveau stockage matériel, il existe 3 types de stockages principaux, quels sont-ils ?
>**les 3 types de stockages sont bloc, fichier et objets**
- 6.Qu’est-ce qu’une base de données et quels sont ses rôles essentiels ?
>**Une base de données constitue un ensemble de donnée structuré dans un certain ordre**
- 7.Quels sont les types de modèles de base de données qui existent ? Citer des exemples.
>**Les types de modèles de base de données sont : relationnelles (ex: MariaDB), orienté objets (ex: Oracle Database), NoSQL (ex: MongoDB)**
- 8.Qu’est-ce qu’un système de gestion de base de données ? Citer des exemples.
>**Un système de gestion de base de données (SGBD) est un logiciel système permettant aux utilisateurs et programmeurs de créer et de gérer des bases de données.**
- 9.Quels sont les différents types de moteurs de SGBD ? Quelles sont leurs différences ?
>**Les diffèrents moteurs de SGBD sont par exemples : MySQL, PostgreSQL, Oracle Database...**
- 10.Quels sont les 3 niveaux d’une architecture classique d’un SGBD ? Détailler.
>**Le SGBD peut être divisé en trois éléments fondamentaux : le dictionnaire de données, le langage de définition des données et le langage de manipulation des données.**
- 11.Quelles sont les étapes pour concevoir une base de données ?
>**1/ Identifier le besoin.
2/ Définir le modèle de base de données.
3/ Choisir le système de gestion de base de données (SGBD)
4/ Prévoir son infrastructure.
5/ Optimiser sa base de données.**
- 12.Comment pouvez-vous modéliser une basede données ? Donner également des exemples d’outils et de méthodes.
>**1) Rédaction des règles de gestion**
**2) Création du dictionnaire des données**
**3) Identification des dépendances fonctionnelles**
**4) Création du Modèle Conceptuel des Données (MCD)**
**5) Création du Modèle Logique des Données (MLD)**
**6) Création du Modèle Physique des Données (MPD)**
- 13.Réaliser la modélisation de l’exemple avec l’illustration de cette page. →
>**1) client_id, client_civ, client_name, client_surname, client_tel, client_ville, client_cp
produit_id, produit_name, produit_sellprice, produit_type,
CREATE TABLE client () ENGINE=INNODB =utf8**
- 14.Quels sont les mécanismes existant pour protéger la donnéeet garantir sa disponibilité ?
>**Faire de la redonnance et des sauvegardes**
- 15.Qu’est ce que le SQL? Celui-ci peut être divisé en 4 sous ensembles, quels sont-ils ?
>**Un language de gestion de base de données**
